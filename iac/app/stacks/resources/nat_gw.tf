# Create Elastic IP
resource "aws_eip" "main" {
  domain = "vpc"
  tags = {
    Name = "eks"
  }
}

# Create NAT Gateway
resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.main.id
  subnet_id     = aws_subnet.public_subnet.id

  tags = {
    Name = "eks"
  }
}

resource "aws_route_table" "b" {
  vpc_id = aws_vpc.custom_vpc.id
  tags = {
    Name = "eks"
  }
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main.id
  }
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.b.id
}

resource "aws_route_table_association" "c" {
  subnet_id      = aws_subnet.private_subnet1.id
  route_table_id = aws_route_table.b.id
}