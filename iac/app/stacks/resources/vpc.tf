resource "aws_vpc" "custom_vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "${var.vpc_tag_name}"
  }
}

data "aws_availability_zones" "available" {}

resource "aws_subnet" "public_subnet" {
  availability_zone       = "us-east-1a"
  cidr_block              = "10.0.1.0/24"
  vpc_id                  = aws_vpc.custom_vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.public_subnet_tag_name}"
  }
}

resource "aws_subnet" "private_subnet" {
  availability_zone = "us-east-1a"
  cidr_block        = "10.0.2.0/24"
  vpc_id            = aws_vpc.custom_vpc.id
  tags = {
    Name = "${var.private_subnet_tag_name}"
  }
}
resource "aws_subnet" "private_subnet1" {
  availability_zone = "us-east-1b"
  cidr_block        = "10.0.3.0/24"
  vpc_id            = aws_vpc.custom_vpc.id
  tags = {
    Name = "${var.private_subnet_tag_name}"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.custom_vpc.id
  tags = {
    Name = "${var.vpc_tag_name}_igw"
  }
}

resource "aws_route_table" "main" {
  vpc_id = aws_vpc.custom_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "${var.vpc_tag_name}_rt"
  }
}

resource "aws_route_table_association" "internet_access" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.main.id
}
