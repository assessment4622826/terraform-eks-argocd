
output "region" {
  value = var.region
}

output "vpc_arn" {
  value = aws_vpc.custom_vpc.arn
}

output "vpc_id" {
  value = aws_vpc.custom_vpc.id
}

output "private_subnet_ids" {
  value = aws_subnet.private_subnet.id
}

output "public_subnet_ids" {
  value = aws_subnet.public_subnet.id
}


output "eks_connect" {
  value = "aws eks --region us-east-1 update-kubeconfig --name ${var.eks_cluster_name}"
}

output "argocd_server_load_balancer" {
  value = data.kubernetes_service.argocd_server.status[0].load_balancer[0].ingress[0].hostname
}

output "argocd_initial_admin_secret" {
  value = "kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath=\"{.data.password}\" | base64 -d"
}