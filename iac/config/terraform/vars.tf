variable "eks_cluster_name" {
  description = "The name of the EKS cluster"
  default     = "main"
  type        = string
}

variable "vpc_tag_name" {
  type        = string
  default     = "main"
  description = "Name tag for the VPC"
}

variable "route_table_tag_name" {
  type        = string
  default     = "main"
  description = "Route table description"
}

variable "vpc_cidr_block" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR block range for vpc"
}


variable "private_subnet_tag_name" {
  type        = string
  default     = "Custom Kubernetes cluster private subnet"
  description = "Name tag for the private subnet"
}

variable "public_subnet_tag_name" {
  type        = string
  default     = "Custom Kubernetes cluster public subnet"
  description = "Name tag for the public subnet"
}


variable "region" {
  type    = string
  default = "us-east-1"
}


variable "min_node_count" {
  type    = number
  default = 3
}

variable "max_node_count" {
  type    = number
  default = 9
}

variable "machine_type" {
  type    = string
  default = "t2.medium"
}


variable "cluster_sg_name" {
  description = "Name of the EKS cluster Security Group"
  default     = "cluster_sg"
  type        = string
}

variable "nodes_sg_name" {
  description = "Name of the EKS node group Security Group"
  default     = "node_sg"
  type        = string
}

