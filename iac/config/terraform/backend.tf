terraform {
  backend "s3" {
    bucket         = "juicy-eks-tfstate"
    key            = "juicy-eks-tfstate/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "juicy-eks-tfstate-table"
    encrypt        = true
  }
}